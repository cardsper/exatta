class MainMenu extends React.Component{
    render(){
        return(
         <div>
             <div className="main-header">
             <div className="header-bottom  header-sticky">
                  <div className="container-fluid">
                      <div className="row align-items-center">
                          {/* <!-- Logo --> */}
                          <div className="col-xl-2 col-lg-1 col-md-1">
                              <div className="logo">
                                <a href="index.html"><img src="assets/img/logo/logo2_footer.png" alt=""/></a>
                              </div>
                          </div>
                          <div className="col-xl-8 col-lg-8 col-md-8">
                              {/* <!-- Main-menu --> */}
                              <div className="main-menu f-right d-none d-lg-block"> 
                                  <nav>
                                      <ul id="navigation">
                                          <li><a id="page1" href="index.html">Início</a></li>
                                          <li><a id="page2"  href="sobre.html">Quem Somos</a></li>
                                          <li><a id="servicos" href="servicos.html">Serviços</a>
                                              <ul className="submenu">
                                                  <li><a id="subpage1" href="gestao_contabil_financeira.html">Gestão contábil &amp; financeira</a></li>
                                                  <li><a id="subpage2" href="reducao_inadimplentes.html">Redução de inadimplentes</a></li>
                                                  <li><a id="subpage3" href="assessoria_sindico.html">Assessoria ao síndicos</a></li>
                                                  <li><a id="subpage4" href="participacao_assembleias.html">Participação em assembleia</a></li>
                                                  <li><a id="subpage5" href="cuidado_condominos.html">Cuidado com os condôminos</a></li>
                                              </ul>
                                          </li>
                                          <li><a id="page3" href="contato.html">Contato</a></li>
                                      </ul>
                                  </nav>                                                                                                                     
                              </div>
                          </div>             
                          <div className="col-xl-2 col-lg-3 col-md-3">
                              <div className="header-right-btn f-right d-none d-lg-block">
                                  <a id="page4" href="https://www.brcondominio.com.br/br1/consulta/index.cfm" className="btn header-btn">ÁREA DO CLIENTE</a>
                              </div>
                          </div>
                          {/* <!-- Mobile Menu --> */}
                          <div className="col-12">
                              <div className="mobile_menu d-block d-lg-none"></div>
                          </div>
                      </div>
                  </div>
             </div>
          </div>
          </div>
       
        )
    }
}
ReactDOM.render(<MainMenu/>, document.getElementById('headerArea'));