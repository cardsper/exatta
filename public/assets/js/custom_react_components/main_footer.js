class MainFooter extends React.Component {
    render() {
        return (
            <div>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"></link>
                <div className="container">
                    <div className="row d-flex justify-content-between">
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div className="single-footer-caption mb-50">
                                <div className="single-footer-caption mb-30">
                                    {/* <!-- logo --> */}
                                    <div className="footer-logo mb-1">
                                        <a href="index.html"><img src="assets/img/logo/logo2_footer.png" alt="" /></a>
                                    </div>
                                    <div className="footer-tittle">
                                        <div className="footer-pera">
                                            <small>Exatta Administradora de Condomínios</small>

                                            <div className="row p-2">
                                                <div className="col-12 p-2"> <a href="https://play.google.com/store/apps/details?id=br.com.brcondominio.app&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img style={{height: '55px'}} src="assets/img/logo/googleplay.jpeg" /></a></div>
                                                <div className="col-12 p-2">  <a href="https://itunes.apple.com/br/app/brcondominio/id1441227069?mt=8"><img style={{height: '55px'}} src="assets/img/logo/playstore.jpeg" /></a></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-2 col-md-4 col-sm-5">
                            <div className="single-footer-caption mb-50">
                                <div className="footer-tittle">
                                    <h4>A Exatta</h4>
                                    <ul>
                                        <li><a href="index.html">Início</a></li>
                                        <li><a href="sobre.html">Quem somos</a></li>
                                        <li><a href="servicos.html">Serviços</a></li>
                                        <li><a href="contato.html">Contato</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                            <div className="single-footer-caption mb-50">
                                <div className="footer-tittle">
                                    <h4>Proporcionamos</h4>
                                    <ul>
                                        <li><a href="gestao_contabil_financeira.html">Gestão Contábil &amp; Financeira</a></li>
                                        <li><a href="reducao_inadimplentes.html">Redução de inadimplentes</a></li>
                                        <li><a href="assessoria_sindico.html">Assessoria ao síndico</a></li>
                                        <li><a href="participacao_assembleias.html">Participação em assembleia</a></li>
                                        <li><a href="cuidado_condominos.html">Cuidado com o condômino</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                            <div className="single-footer-caption mb-50">
                                <div className="footer-tittle">
                                    <h4>Entre em contato</h4>
                                    <ul>
                                        <li><a href="mailto:atendimento@exattaadm.com"></a></li>
                                        <li><a href="contato.html">Asa Sul, Brasília</a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=+556133210449">+55 (61) 3321-0449  <i style={{ color: "#25D336" }} className="fa fa-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {/* // <!-- footer-bottom aera --> */}
                <div className="footer-bottom-area footer-bg">
                    <div className="container">
                        <div className="footer-border">
                            <div className="row d-flex align-items-center">
                                <div className="col-xl-12 ">
                                    <div className="footer-copy-right text-center">
                                        <p>Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script> Todos os direitos
                            reservados | Exatta Administradora de Condomínios</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="newsletterFeedbackModal" className="modal fade" role="dialog">
        <div className="modal-dialog">
      

          <div className="modal-content py-5 px-3">
            <div className="modal-body text-center">
                <i className="fas fa-check fa-3x"></i>
              <h3>Solicitação enviada com sucesso!</h3>
              <h6 className="mt-3 text-muted">Em breve entraremos em contato seja através de seu e-mail, telefonema ou Whatsapp.</h6>
              <div className="col-12 text-center mt-4">
                <button type="button" className="btn btn-sm btn-default" data-dismiss="modal">Ok</button>
            </div>
            </div>
           
            </div>
      
        </div>
      </div>

            </div>


        )
    }

}
ReactDOM.render(<MainFooter />, document.getElementById('main-footer'));