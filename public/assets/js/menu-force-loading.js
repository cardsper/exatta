jQuery(document).ready(function($){
    setTimeout(function () {
        $('#navigation').slicknav({
            prependTo: ".mobile_menu",
            closedSymbol: '+',
            openedSymbol: '-'
        });
    }, 500);
});