
$(document).ready(function() {

    $("form").each(function(){
        $(this).submit(function(e){
            e.preventDefault();
            var $this = $(this);
            var values = new FormData(this);
            values.append('id', $this.attr('id'));
            var loadingIco = $this.find('i');
            var contactFormBtnTxt = $this.find('.button-contactForm span');
            var contactFeedbackModal = $("#contactFeedbackModal");
            var newsletterFeedbackModal = $("#newsletterFeedbackModal");
            var formType = "";
            $this.hasClass('newsLetter') ? formType = "newsletter" : formType = "contact";
            $.ajax({
                type: "POST",
                data: values,
                dataType: 'json',
                processData:false,
                contentType:false,
                cache: false,
                url: $this.attr("action"),
                cache: false,
                beforeSend: function(){      
                        contactFormBtnTxt.text("Enviando...");
                        loadingIco.toggleClass("d-none");   
                },
                success: function(){
                
                }
            })
            .done(function(){

                contactFormBtnTxt.text("Enviar");
                loadingIco.toggleClass("d-none");
                 if(formType == 'contact'){
                    contactFeedbackModal.modal('show');  
                }
                else{
                    newsletterFeedbackModal.modal('show');
                }
            })
            .fail(function(){
                contactFormBtnTxt.text("Enviar");
                if(formType == 'contact'){
                    console.log("Erro ao enviar mensagem do formulario de contato.");   
                }
                else{
                    console.log("Erro ao enviar cadastro do formulario de newsletter.");   
                }
            });
        });
    });

    $("form.newsLetter").each(function(){
        $(this).submit(function(e){
            e.preventDefault();
           var $this = $(this);
           var values = $this.serialize();
        //    values.append('id', $this.attr('id'));
           console.log(values);
        });
    });

    });





    