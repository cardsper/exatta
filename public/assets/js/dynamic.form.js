const form1 = document.getElementById("contactForm1");
const form2 = document.getElementById("contactForm2");
const form3 = document.getElementById("contactForm3");
const form4 = document.getElementById("contactForm4");
var inputAbout = document.getElementById("contactForm2_assunto");
var formSelectionReason = document.getElementById("motivo-select");



function changeMotivoForm(){
  
    function setFormFunction(i){
        var forms = {
            '0' : () => { updateForm(form1)},
            '1' : () => { updateForm(form2)},
            '2' : () => { updateForm(form2)},
            '3' : () => { updateForm(form2)},
            '4' : () => { updateForm(form2)},
            '5' : () => { updateForm(form3)},
            '6' : () => { updateForm(form4)},
        };
        return forms[i];
    }
    
    var form = setFormFunction(formSelectionReason.value);
    form();

    var aboutTextValue = formSelectionReason.options[formSelectionReason.selectedIndex].innerHTML;
    inputAbout.value = aboutTextValue;

    console.log(aboutTextValue);
}

 function updateForm(f){
    var selectedForm = f;  
   
    var otherForm = document.getElementsByClassName("form-contact");
    for (let i = 0; i < otherForm.length; i++) {
             otherForm[i].style.display = "none";
             selectedForm.style.display = "block";
            }

    return selectedForm;
}


